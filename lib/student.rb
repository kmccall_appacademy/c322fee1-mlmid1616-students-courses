
require 'course'

class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{first_name} #{last_name}"
  end

  # def enroll(course)
  #   @courses << course
  #   course.add_student(self)
  # end

  def enroll(new_course)
    raise "error" if self.courses.any? do |course| new_course.conflicts_with?(course) end  
    self.courses << new_course unless self.courses.include?(new_course)
    new_course.students << self
  end

  # def course_load
  # totalload = {}
  # @courses.each do |course|
  #   if totalload.keys.include?(course.department)
  #     totalload[course.department] += course.credits
  #   else
  #     totalload[course.department] = course.credits
  #   end
  # end
  # totalload
  # end

  def course_load
    current_credits = Hash.new
    @courses.each do |course|
      if current_credits[course.department]
        current_credits[course.department] += course.credits
      else
        current_credits[course.department] = course.credits
      end
    end
    current_credits
  end




end
